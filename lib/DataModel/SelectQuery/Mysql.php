<?php
namespace DataModel\SelectQuery;

class Mysql extends \DataModel\SelectQuery {
	private $_sql = array(
		'fields' => null,
		'from' => null,
		'where' => null,
		'orderby' => null,
		'limit' => null
	);

	private $_schema = array(
		'Artists' => array(
			'table' => 'releases',
			'use_distinct' => true,
			'fields' => array(
				'id' => 'artist_id',
				'name' => 'artist_name',
			),
		),
	);

	public function setFilter($filter) {
	}

	public function setSorter($sorter) {
	}

	public function setPager($pager) {
		$this->_sql['limit'] = $pager->start() . ', ' . $pager->count();
	}

	public function all() {
		if ($this->_sql['fields'] === null) {
			$this->_sql['fields'] = implode(', ', $this->_schema[$this->_source]['fields']);
		}
		if ($this->_sql['from'] === null) {
			$this->_sql['from'] = $this->_schema[$this->_source]['table'];
		}
		if ($this->_sql['limit'] === null) {
			throw new Exception('No pager set');
		}

		$distinct = ($this->_schema[$this->_source]['use_distinct']) ? 'DISTINCT ' : '';

		$sql = "SELECT $distinct{$this->_sql['fields']} "
			 . "FROM {$this->_sql['from']} "
			 . "LIMIT {$this->_sql['limit']}";

		return $this->_dataStore->all($sql);
	}
}

