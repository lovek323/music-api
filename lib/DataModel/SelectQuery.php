<?php
namespace DataModel;

abstract class SelectQuery {
	protected $_dataStore = null;
	protected $_source = null;

	public function __construct($dataStore) {
		$this->_dataStore = $dataStore;
	}

	public function setSource($source) {
		$this->_source = $source;
	}

	abstract function setFilter($filter);
	abstract function setSorter($sorter);
	abstract function setPager($pager);

	abstract function all();
}

