<?php
require_once 'config.php';

require_once 'lib/Sorter.php';
require_once 'lib/Pager.php';

require_once 'lib/Repository.php';
require_once 'lib/Repository/Artists.php';

require_once 'lib/DataModel/SelectQuery.php';
require_once 'lib/DataModel/SelectQuery/Mysql.php';
require_once 'lib/DataModel/SelectQuery/Factory/Epiphany.php';

include_once 'epiphany/src/Epi.php';

Epi::setPath('base', 'epiphany/src');
Epi::init('api', 'route', 'database');
EpiDatabase::employ('mysql', 'nzb_music', 'localhost', 'root', DATABASE_PASSWORD);

getRoute()->get('/', 'showEndpoints');

getApi()->get('/version.json', 'apiVersion', EpiApi::external);
getApi()->get('/artists.json', 'artists', EpiApi::external);
// getApi()->get('/releases.json', 'releases', EpiApi::external);
// getApi()->get('/statistics.json', 'statistics', EpiApi::external);

getRoute()->run();

function showEndpoints() {
  echo '<ul>
          <li><a href="/">/</a> -> (home)</li>
          <li><a href="/version.json">/version.json</a></li>
		  <li><a href="/releases.json">/releases.json</a></li>
		  <li><a href="/statistics.json">/statistics.json</a></li>
        </ul>';
}

function showVersion() { echo 'The version of this api is: ' . getApi()->invoke('/version.json'); }

function apiVersion() { return '0.1'; }

function error($message) { exit($message); }

function artists() {
	$artists = new Repository\Artists();
	return $artists->get();
}

function showReleases() {
	$start = 0;
	if (!empty($_GET['start'])) {
		$start = $_GET['start'];
		if ((int)$start == 0) error("$start is not a valid start index");
	}

	$count = 10;
	if (!empty($_GET['count'])) {
		$count = $_GET['count'];
		if ((int)$count == 0 || (int)$count > 100) error("$count is not a valid result count");
	}

	$filter = array();
	$_filter = array();
	if (!empty($_GET['filter'])) {
		$filter = $_GET['filter'];
		if (!is_array($filter)) error("$filter is not a valid filter");
		foreach ($filter as $k => $v) {
			switch ($k) {
				case 'scoreAtLeast':
					if (!(float)$v) error("$v is not a valid value for filter scoreAtLeast");
					$_filter[$k] = array(
						'field' => 'score',
						'operator' => '>=',
						'value' => (float)$v
					);
					break;
				case 'artistName':
					$_filter[$k] = array(
						'field' => 'artist_name',
						'operator' => 'LIKE',
						'value' => $v
					);
					break;
				default:
					error("$k is not a valid filter");
			}
		}
	}

	$sql = "SELECT * FROM releases WHERE 1=1";
	$countSql = "SELECT COUNT(*) c FROM releases WHERE 1=1";
	$whereParams = array();
	foreach ($_filter as $k => $v) {
		$sql .= " AND {$v['field']} {$v['operator']} :$k";
		$countSql .= " AND {$v['field']} {$v['operator']} :$k";
		$whereParams[":$k"] = $v['value'];
	}
	$sql .= " ORDER BY `$sortField` $sortDirection LIMIT $start, $count";
	$countSql .= " ORDER BY `$sortField` $sortDirection LIMIT $start, $count";
	$retval = new stdClass();
	$retval->releases = getDatabase()->all($sql, $whereParams);
	$retval->count = getDatabase()->one($countSql, $whereParams);
	$retval->count = (int)$retval->count['c'];
	return $retval;
}

