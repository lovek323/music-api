<?php
namespace Repository;

class Artists extends \Repository {
	protected $_filterFields = array(
		'id',
		'name'
	);

	protected $_sortFields = array(
		'id',
		'name'
	);

	public function get() {
		$selectQuery = $this->_selectQueryFactory->create();
		$selectQuery->setSource('Artists');
		$selectQuery->setFilter($this->getFilter());
		$selectQuery->setSorter($this->getSorter());
		$selectQuery->setPager($this->getPager());
		return $selectQuery->all();
	}
}

