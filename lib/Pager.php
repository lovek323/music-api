<?php
class Pager {
	protected $_count;
	protected $_start;

	public function __construct($count, $start) {
		$this->_count = $count;
		$this->_start = $start;
	}

	public function count() {
		return $this->_count;
	}

	public function start() {
		return $this->_start;
	}
}

