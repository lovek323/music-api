<?php
namespace DataModel\SelectQuery;

abstract class Factory {
	abstract function create();
}

