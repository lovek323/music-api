<?php
abstract class Repository {
	protected $_selectQueryFactory;

	protected $_sortFields = array();
	protected $_defaultSortField;
	protected $_defaultSortDirection;

	protected $_maxPageSize = DEFAULT_MAX_PAGE_SIZE;
	protected $_defaultPageSize = DEFAULT_PAGE_SIZE;

	protected $_filterFields = array();

	public function __construct($selectQueryFactory = null) {
		if (($this->_selectQueryFactory = $selectQueryFactory) === null) {
			$this->_selectQueryFactory = new DataModel\SelectQuery\Factory\Epiphany();
		}
	}

	protected function getSorter() {
		$field = $this->_defaultSortField;
		$direction = $this->_defaultSortDirection;
		if (!empty($_GET['sort'])) {
			$sort = $_GET['sort'];
			if (!empty($sort['field'])) {
				$field = $sort['field'];
				if (!in_array($field, $_sortFields)) throw new Exception("Invalid sort field: $field");
			}
			if (!empty($sort['direction'])) {
				$direction = $sort['direction'];
				if (!in_array($direction, array('asc', 'desc'))) throw new Exception("Invalid sort direction: $direction");
			}
		}
		return new Sorter($field, $direction);
	}

	protected function getPager() {
		$count = $this->_defaultPageSize;
		$start = 0;
		if (!empty($_GET['pager'])) {
			$pager = $_GET['pager'];
			if (!empty($pager['count'])) {
				$count = (int)$pager['count'];
				if (!$count || $count > $this->_maxPageSize || $count < 0) throw new Exception("Invalid pager count: $count");
			}
			if (!empty($pager['start'])) {
				$start = (int)$pager['start'];
				if ($start < 0) throw new Exception("Invalid pager start: $start");
			}
		}
		return new Pager($count, $start);
	}

	protected function getFilter() {
	}

	protected function getInfo() {
		return '<p>Not implemented</p>';
	}

	protected function findMatching() {
	}
}

